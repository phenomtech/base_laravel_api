<?php
/**
 * Modified by PhpStorm.
 * User: PooryaBagheriFaez
 * Date: 10/15/2018
 * Time: 5:05 PM
 */
namespace App\Exceptions;

use App\Models\ErrorModel;
use Dotenv\Validator;
use Exception;
use App\Traits\ApiResponser;
use Asm89\Stack\CorsService;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;


class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $response = $this->handleException($request, $exception);

        app(CorsService::class)->addActualRequestHeaders($response, $request);

        return $response;

    }

    public function handleException($request, Exception $exception)
    {
        if ($exception instanceof PostTooLargeException) {
            return $this->errorResponse(new ErrorModel("The file may not be greater than 8000 kilobytes."), 413);
        }

        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        if ($exception instanceof ModelNotFoundException) {
            $modelName = strtolower(class_basename($exception->getModel()));

            return $this->errorResponse(new ErrorModel("No '{$modelName}' exists with the specified identifier."), 404);
        }

        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse(new ErrorModel($exception->getMessage()), 403);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse(new ErrorModel('The specified method for the request is invalid'), 405);
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse(new ErrorModel('The specified URL cannot be found'), 404);
        }

        if ($exception instanceof HttpException) {
            return $this->errorResponse(new ErrorModel($exception->getMessage()), $exception->getStatusCode());
        }

        if ($exception instanceof QueryException) {
            $errorCode = $exception->errorInfo[1];

            if ($errorCode == 1451) {
                return $this->errorResponse(new ErrorModel('Cannot remove this resource permanently. It is related with any other resource'), 409);
            }
        }

        /***
         *file size validation handler on exception level
         **/
        if ($exception instanceof FileException) {
            // create a validator and validate to throw a new ValidationException
            return Validator::make($request->all(), [
                'your_file_input' => 'required|file|size:8000',
            ])->validate();
        }


        if ($exception instanceof TokenMismatchException) {
            return redirect()->back()->withInput($request->input());
        }

        if (config('app.debug')) {
            return parent::render($request, $exception);
        }

        try {
            Log::critical('Server has thrown ERROR 500. REQUEST : ' . $request . ' EXCEPTION : ' . $exception->getMessage());
        } catch (Exception $e) {

        }
        return $this->errorResponse(new ErrorModel('Unexpected Exception. Try later.'), 500);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($this->isFrontend($request)) {
            return redirect()->guest('login');
        }

        return $this->errorResponse(new ErrorModel($exception->getMessage()), 401);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException $e
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        if ($this->isFrontend($request)) {
            return $request->ajax() ? response()->json($e, 422) : redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($errors);
        }

        $formattedError = [];
        foreach ($errors as $key => $message) {
            $formattedError[] = new ErrorModel($message[0], $key);
        }

        return $this->errorResponse($formattedError, 422);
    }

    private function isFrontend($request)
    {
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
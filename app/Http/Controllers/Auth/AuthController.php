<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Traits\ApiResponser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class AuthController extends Controller
{
    use ApiResponser;

    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(AuthLoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $tokenResult = $user->createToken('MyApp');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            $data['token_type'] = "Bearer";
            $data['token'] = $tokenResult->accessToken;
            $data['expires_in'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString($token->expires_at);
            return $this->showAll(collect($data));
        } else {
            return $this->authorizationError();
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(AuthRegisterRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);


        if (User::where('email', $input['email'])->exists()) {
            $error['email']='There is a record with this email';
            return $this->formErrorValidation($error);
        }

        try{
            $user = User::create($input);
        }catch(\Exception $e){
            return $this->conflictError("something went wrong");
        }

        $tokenResult = $user->createToken('MyApp');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $data['token_type'] = "Bearer";
        $data['token'] = $tokenResult->accessToken;
        $data['expires_in'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString($token->expires_at);


        return $this->showAll(collect($data));
    }
}

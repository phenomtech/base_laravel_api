<?php
/**
 * Created by PhpStorm.
 * User: PooryaBagheriFaez
 * Date: 10/16/2018
 * Time: 6:32 PM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class AuthRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|max:255',
            'c_password' => 'required|string|max:255|same:password',
        ];
    }
}
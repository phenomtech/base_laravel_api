<?php
/**
 * Created by PhpStorm.
 * User: PooryaBagheriFaez
 * Date: 10/15/2018
 * Time: 5:05 PM
 */

namespace App\Models;


class ErrorModel
{
    public $field;
    public $message;

    public function __construct($message, $field = 'Exception')
    {
        $this->field = $field;
        $this->message = $message;
    }
}
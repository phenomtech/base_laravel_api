<?php
/**
 * Created by PhpStorm.
 * User: PooryaBagheriFaez
 * Date: 5/21/2018
 * Time: 6:32 PM
 */

namespace App\Traits;

use App\Models\ErrorModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

////use TestSerializer;
//use App\TestSerializer;

trait ApiResponser
{
    function successResponse($data, $code = 200)
    {
        return response()->json($data, $code);
    }

    function errorResponse($error, $code)
    {
        if (is_array($error)) {
            $formattedResponse = [];
            foreach ($error as $e) {
                $formattedResponse[] = $e;
            }

            return response()->json([
                'error' => $formattedResponse
            ], $code);
        }

        if ($error instanceof ErrorModel) {
            return response()->json([
                'error' => [[
                    'field' => $error->field,
                    'message' => $error->message
                ]]
            ], $code);
        }

        if (is_string($error)) {
            return response()->json([
                'error' => [[
                    'field' => 'Exception',
                    'message' => $error
                ]]
            ], $code);
        }
    }

    function OK($message = "OK", $status_code = 200)
    {
        return $this->successResponse(['message' => $message], $status_code);
    }

    function deleteSuccess($message = "Resource Deleted Successfully", $status_code = 204)
    {
        return $this->successResponse(['message' => $message], $status_code);
    }

    function notFoundError($message = "Request not found", $status_code = 404, $field = "exception")
    {
        return $this->errorResponse(new ErrorModel($message, $field), $status_code);
    }

    function forbiddenError($message = "Forbidden request", $status_code = 403)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);
    }

    function serverError($message = "Internal server error", $status_code = 500)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);
    }

    function badRequest($message = 'Malformed Request', $status_code = 400)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);

    }

    function authorizationError($message = "Unauthorized action", $status_code = 401)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);
    }

    function conflictError($message = "Conflict Error", $status_code = 409)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);
    }


    function validationError($message = "Validation Error", $status_code = 422)
    {
        return $this->errorResponse(new ErrorModel($message), $status_code);
    }

    function formErrorValidation($messages, $status_code = 422)
    {
        $messagesList = [];
        $messagesList = $this->fooFormatter($messages);

        return $this->errorResponse(new ErrorModel($messagesList['message'], $messagesList['field']), 422);
    }

    /**
     * @param $messages
     * @return array
     */
    public static function fooFormatter($messages)
    {
        foreach ($messages as $atr => $msg) {
            $messagesList = [
                'field' => $atr,
                'message' => $msg
            ];
        }
        return $messagesList;
    }

    /**
     * @param Collection $collection
     * @param int $code : response code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAllPaginated(Collection $collection, $code = 200)
    {
        return $this->showAll($collection, $code, true);
    }

    /**
     * @param Collection $collection
     * @param int $code :response code
     * @param bool $isPaginated enable pagination (disabled by default)
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAll(Collection $collection, $code = 200, $isPaginated = false)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }

        $collection = $this->filterData($collection);

        $collection = $this->sortData($collection);

        if ($isPaginated) {
            $collection = $this->paginate($collection);
        }

//        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200)
    {
        return $this->successResponse($instance, $code);
    }

    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse(['data' => $message], $code);
    }

    protected function filterData(Collection $collection)
    {
        foreach (request()->query() as $query => $value) {


            $attribute = $query;
            if (isset($attribute, $value)) {
                $collection = $collection->where($attribute, $value);
            }
        }

        return $collection;
    }

    protected function sortData(Collection $collection)
    {
        if (request()->has('sort_by')) {
            $attribute = request()->sort_by;
            $collection = $collection->sortBy->{$attribute};
        }

        return $collection;
    }


    protected function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:2|max:50',
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 10;
        if (request()->has('per_page')) {
            $perPage = (int)request()->per_page;
        }

        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        $paginated->appends(request()->all());

        return $paginated;

    }

    protected function cacheResponse($data)
    {
        $url = request()->url();
        $queryParams = request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = "{$url}?{$queryString}";

        return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
            return $data;
        });
    }
}
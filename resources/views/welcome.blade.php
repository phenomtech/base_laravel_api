<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>base-api</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
    {{--<app>--}}
        {{--<example-component>--}}
        {{--<passport-clients></passport-clients>--}}
        {{--<passport-authorized-clients></passport-authorized-clients>--}}
        {{--<passport-personal-access-tokens></passport-personal-access-tokens>--}}
    {{--</app>--}}
</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
<?php

namespace Tests;

use App\Checkout\Ads\Ad;
use App\Checkout\Deals\Deal;
use App\Checkout\Deals\DiscountDeal;
use App\Checkout\Deals\MultibuyDeal;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

use Faker\Factory as Faker;


abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, DatabaseTransactions;

    protected $faker;
    protected $user;

    /**
     * Set up the test
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        /** setup Users */
        $this->user = factory(User::class)->create([
            'name' => 'User',
            'email' => 'ads@user.test'
        ]);


    }

    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
